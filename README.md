# Bienvenidos al repositorio del Turnero

Este proyecto inicio en el año 2022 como parte del Bootcamp de Desarrollo Web-Fullstack impartido por
la Facultad Politecnica de la Universidad Nacional de Asuncion. Este documento describe los requerimientos necesarios para levantar el proyecto en un entono local para
poder evaluar su funcionamiento.

# Instaladores

##### 1) Compilador

- [Python3](https://www.python.org/downloads/release/python-396/ "Python3")

##### 2) Editor de Texto

- [Visual Studio Code](https://code.visualstudio.com/ "Visual Studio Code")

##### 3) Motor de base de datos

- [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads "PostgreSQL")

##### 4) Administrador de Versiones

- [Git](https://git-scm.com/downloads "Git")

# Clonar Repositorio, crear Entorno Virtual, Instalar la Librerias, Crear las Migraciones y Ejecutar el servidor de web.

##### 1) Clonar o descargar el proyecto del repositorio

`git clone https://gitlab.com/bootcamp327/proyecto_turnero.git`

##### 2) Crear un entorno virtual para posteriormente instalar las librerias del proyecto

- `python3 -m venv venv` (Windows)

##### 3) Activar el entorno virtual de nuestro proyecto

- `cd venv\Scripts\activate.bat` (Windows)

##### 5) Instalar todas las librerias del proyecto

- `pip install -r requirements.txt`

##### 6) Crear la base de datos con las migraciones y el superuser para iniciar sesión

- `python manage.py makemigrations`
- `python manage.py migrate`
- `python manage.py createsuperuser`

##### 7) Insertar información inicial en la base de datos

- `C:\Program Files\PostgreSQL\14\bin\psql -U postgres -d baselocal -f "turnero.sql"`

##### 8) Ejecutar el servidor del proyecto.

- `python manage.py runserver`
------------

🤗💪¡Muchas Gracias!💪🤗
