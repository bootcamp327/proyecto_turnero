--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: TipoClientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."TipoClientes" VALUES (1, 'TITULAR CUENTA', '2023-02-26 23:23:36.728985-03', NULL, '2023-02-26 23:23:35-03', NULL);
INSERT INTO public."TipoClientes" VALUES (2, 'OCASIONAL', '2023-02-26 23:23:51.97815-03', NULL, '2023-02-26 23:23:51-03', NULL);
INSERT INTO public."TipoClientes" VALUES (3, 'PROVEEDOR', '2023-02-27 16:24:17.983138-03', NULL, '2023-02-27 16:24:17-03', NULL);
INSERT INTO public."TipoClientes" VALUES (4, 'EMPLEADO', '2023-02-27 16:24:30.383398-03', NULL, '2023-02-27 16:24:29-03', NULL);

--
-- Data for Name: TipoDocumentos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."TipoDocumentos" VALUES (2, 'CEDULA IDENTIDAD', '2023-02-26 23:22:33.036452-03', NULL, '2023-02-26 23:22:32-03', NULL);
INSERT INTO public."TipoDocumentos" VALUES (3, 'PASAPORTE', '2023-02-26 23:22:43.841434-03', NULL, '2023-02-26 23:22:42-03', NULL);

--
-- Data for Name: Clientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Clientes" VALUES (2, '221522-55', 'MARIA ISABEL', 'SANTACRUZ', 'PALMA 314 C/ALBERDI', '0991 788063', 'mariasatacruz@gmail.com', '2023-02-26 23:24:53.757117-03', NULL, '2023-02-26 23:24:52-03', NULL, 2, 3);
INSERT INTO public."Clientes" VALUES (1, '2123552', 'JOSE CARLOS', 'MACUELLO', '225 DE MAYO 1655', '021 215-552', 'josemancuello@gmail.com', '2023-02-26 23:24:01.400217-03', NULL, '2023-02-26 23:23:59-03', NULL, 1, 2);
INSERT INTO public."Clientes" VALUES (3, '3552885', 'JOSEFINA', 'MARTINEZ ORUE', 'MANUEL ORTIZ GUERRERO 1255', '021 558-963', 'jpsefinaorue@hotmail.com', '2023-02-27 16:26:10.190324-03', NULL, '2023-02-27 16:26:08-03', NULL, 3, 2);

--
-- Data for Name: Prioridades; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Prioridades" VALUES (1, 'ALTA', '2023-02-26 23:17:52.75191-03', NULL, '2023-02-26 23:17:51-03', NULL);
INSERT INTO public."Prioridades" VALUES (2, 'MEDIA', '2023-02-26 23:18:27.271228-03', NULL, '2023-02-26 23:18:26-03', NULL);
INSERT INTO public."Prioridades" VALUES (3, 'BAJA', '2023-02-26 23:18:35.900495-03', NULL, '2023-02-26 23:18:34-03', NULL);

--
-- Data for Name: Condiciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Condiciones" VALUES (1, 'EMBARAZADA', 'EMBARAZADA', '2023-02-26 23:17:59.949136-03', NULL, '2023-02-26 23:17:40-03', NULL, 1);
INSERT INTO public."Condiciones" VALUES (2, 'DISCAPACIDAD MOVIL', 'DISCAPACIDAD MOVIL', '2023-02-26 23:18:46.717949-03', NULL, '2023-02-26 23:18:45-03', NULL, 1);
INSERT INTO public."Condiciones" VALUES (3, 'TERCERA EDAD', 'TERCERA EDAD', '2023-02-26 23:19:04.494774-03', NULL, '2023-02-26 23:19:01-03', NULL, 1);
INSERT INTO public."Condiciones" VALUES (5, 'SIN CONDICION', 'SIN CONDICION', '2023-02-26 23:19:53.141381-03', NULL, '2023-02-26 23:19:47-03', NULL, 3);
INSERT INTO public."Condiciones" VALUES (6, 'DISCAPACIDAD PARCIAL', 'DISCAPACIDAD PARCIAL', '2023-02-27 17:07:57.06181-03', NULL, '2023-02-27 17:07:53-03', NULL, 2);

--
-- Data for Name: Estados; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Estados" VALUES (2, 'PENDIENTE', '2023-02-26 23:17:16.496626-03', NULL, '2023-02-26 23:17:14-03', NULL);
INSERT INTO public."Estados" VALUES (1, 'FINALIZADO', '2023-02-26 23:17:05.786252-03', NULL, '2023-02-26 23:17:03-03', NULL);
INSERT INTO public."Estados" VALUES (3, 'ANULADO', '2023-02-27 16:21:26.805959-03', NULL, '2023-02-27 16:21:24-03', NULL);

--
-- Data for Name: Roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Roles" VALUES (1, 'OPERADOR', 'O', '2023-02-26 16:53:32.911193-03', NULL, '2023-02-26 16:53:31-03', NULL);
INSERT INTO public."Roles" VALUES (2, 'ADMINISTRACION', 'A', '2023-02-26 16:53:45.403117-03', NULL, '2023-02-26 16:53:44-03', NULL);
INSERT INTO public."Roles" VALUES (3, 'RECEPCIONISTA', 'R', '2023-02-26 16:53:59.72302-03', NULL, '2023-02-26 16:53:58-03', NULL);

--
-- Data for Name: Servicios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Servicios" VALUES (2, 'AREA COMERCIAL', 'AREA COMERCIAL', '2023-02-26 23:20:41.873954-03', NULL, '2023-02-26 23:20:40-03', NULL);
INSERT INTO public."Servicios" VALUES (3, 'ENTREGA DOCUMENTOS', 'ENTREGA DOCUMENTOS', '2023-02-26 23:20:59.061683-03', NULL, '2023-02-26 23:20:57-03', NULL);
INSERT INTO public."Servicios" VALUES (1, 'CAJAS', 'CAJAS', '2023-02-26 23:20:26.063772-03', NULL, '2023-02-26 23:20:24-03', NULL);
INSERT INTO public."Servicios" VALUES (4, 'PAGO A PROVEEDORES', 'PAGO A PROVEEDORES', '2023-02-27 16:26:50.824233-03', NULL, '2023-02-27 16:26:49-03', NULL);
