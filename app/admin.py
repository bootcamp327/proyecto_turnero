from django.contrib import admin
from .models import Tipodocumentos, Prioridades, Condiciones, Estados, Servicios, Tipoclientes, Roles, Clientes, Turnos

# Register your models here.
class TipodocumentosAdmin(admin.ModelAdmin):
    list_display  = ["tipo_documento_id","descripcion","fecha_alta","usuario_alta","fecha_cambio","usuario_cambio"]
    search_fields = ["descripcion","usuario_alta"]

class PrioridadesAdmin(admin.ModelAdmin):
    list_display  = ["prioridad_id","descripcion","fecha_alta","usuario_alta","fecha_cambio","usuario_cambio"]
    search_fields = ["descripcion","usuario_alta"]

class CondicionesAdmin(admin.ModelAdmin):
    list_display  = ["condicion_id","descripcion","abreviatura","prioridad","fecha_alta"]
    search_fields = ["descripcion"]

class TipoclientesAdmin(admin.ModelAdmin):
    list_display  = ["tipo_cliente_id","descripcion","fecha_alta","usuario_alta"]
    search_fields = ["descripcion"]

class EstadosAdmin(admin.ModelAdmin):
    list_display  = ["estado_id","descripcion","fecha_alta","usuario_alta"]
    search_fields = ["descripcion"]    

class RolesAdmin(admin.ModelAdmin):
    list_display  = ["rol_id","descripcion","tipo_rol","fecha_alta","usuario_alta"]
    search_fields = ["descripcion"]  

class ServiciosAdmin(admin.ModelAdmin):
    list_display  = ["servicio_id","descripcion","abreviatura","fecha_alta","usuario_alta"]
    search_fields = ["descripcion"]  

class ClientesAdmin(admin.ModelAdmin):
    list_display  = ["cliente_id","ndocumento", "nombres","apellidos","correos","fecha_alta","usuario_alta"]
    search_fields = ["ndocumento","nombres","apellidos"]  

class TurnosAdmin(admin.ModelAdmin):
    list_display  = ["turno_id","nticket", "fecha","hora","cliente","servicio","comentario"]
    search_fields = ["nticket","cliente","servicio"]  

admin.site.register(Tipodocumentos, TipodocumentosAdmin)
admin.site.register(Prioridades, PrioridadesAdmin)
admin.site.register(Condiciones, CondicionesAdmin)  
admin.site.register(Tipoclientes, TipoclientesAdmin) 
admin.site.register(Estados, EstadosAdmin)
admin.site.register(Roles, RolesAdmin)
admin.site.register(Servicios, ServiciosAdmin)
admin.site.register(Clientes, ClientesAdmin)
admin.site.register(Turnos, TurnosAdmin)