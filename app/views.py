from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError

from .forms import TurnoForm
from .models import Turnos

# Create your views here.
def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'GET':
        return render(request, 'signup.html', {'form': UserCreationForm})
    else:
        # Register user
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
                user.save()
                login(request, user)
                return redirect('home')
            except IntegrityError:
                return render(request, 'signup.html', {
                    'form': UserCreationForm, 
                    'error': 'Cuenta de Usuario ya existente!'})
        return render(request, 'signup.html', {
            'form': UserCreationForm, 
            'error': 'La contraseña invalida!'})

@login_required
def signout(request):
    logout(request)
    return redirect('home')

def signin(request):
    if request.method == 'GET':
        return render(request, 'signin.html', {
            'form': AuthenticationForm
        })
    else:
        user = authenticate(request, username = request.POST['username'], password = request.POST['password'])
        if user is None:
            return render(request, 'signin.html', {'form': AuthenticationForm, 'error': 'Cuenta de Usuario o Contraseña es Incorrecta!'})
        else:
            login(request, user)
            return redirect('home')

@login_required
def crear_turno(request):
    if request.method == 'GET':
        return render(request, 'crear_turno.html', {'form': TurnoForm})
    else:   
        try:
            turno_form = TurnoForm(request.POST)
            new_turno = turno_form.save(commit=False)
            new_turno.usuario = request.user
            new_turno.save()
            return redirect('turnos_pendientes')

        except ValueError:   
            return render(request, 'crear_turno.html', {
                'form': turno_form,
                'Error': 'Por favor, proveer datos validos!'
            })

    #turno_form = TurnoForm()
    #return render(request, 'crear_turno.html', {'form': turno_form})

@login_required
def turnos_pendientes(request):
    turnos = Turnos.objects.all()
    print(turnos)
    return render(request, 'turnos_pendientes.html', {'turnos': turnos})