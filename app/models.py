# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

Tipo_rol_choices = (("A","Administrador"),("O","Operador"),("R","Recepcionista"))

class Tipoclientes(models.Model):
    tipo_cliente_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=30, blank=True, null=True)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'TipoClientes'
        verbose_name_plural = "Tipos de Clientes"

    def __str__(self):
        return self.descripcion 

class Tipodocumentos(models.Model):
    tipo_documento_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=255, blank=False, null=True)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'TipoDocumentos'
        verbose_name_plural = "Tipos de Documentos"

    def __str__(self):
        return self.descripcion 

class Clientes(models.Model):
    cliente_id = models.AutoField(primary_key=True)
    tipo_documento = models.ForeignKey(Tipodocumentos, models.CASCADE, verbose_name='Tipo de Documento')
    ndocumento = models.CharField(unique=True, max_length=20, blank=True, null=True, verbose_name='Nº de Documento')
    nombres = models.CharField(max_length=30, blank=True, null=True, verbose_name='Nombres')
    apellidos = models.CharField(max_length=30, blank=True, null=True, verbose_name='Apellidos')
    direccion = models.CharField(max_length=50, blank=True, null=True, verbose_name='Direccion')
    telefonos = models.CharField(max_length=50, blank=True, null=True, verbose_name='Telefonos')
    correos = models.CharField(max_length=50, blank=True, null=True, verbose_name='Correo Electronico')
    tipo_cliente = models.ForeignKey('Tipoclientes', models.CASCADE, blank=True, null=True, verbose_name='Tipo de Cliente')
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True, verbose_name='Fecha de Alta')
    usuario_alta = models.CharField(max_length=30, blank=True, null=True, verbose_name='Usuario de Alta')
    fecha_cambio = models.DateTimeField(blank=True, null=True, verbose_name='Ultima Fecha de Cambio')
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True, verbose_name='Usuario de Ultimo Cambio')

    class Meta:
        db_table = 'Clientes'
        verbose_name_plural = "Clientes"

    def __str__(self):
        return self.nombres + ', ' + self.apellidos 

class Prioridades(models.Model):
    prioridad_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50, blank=True, null=True)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'Prioridades'
        verbose_name_plural = "Prioridades"

    def __str__(self):
        return self.descripcion

class Condiciones(models.Model):
    condicion_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    abreviatura = models.CharField(max_length=30)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)
    prioridad = models.ForeignKey('Prioridades', models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'Condiciones'
        verbose_name_plural = "Condiciones"

    def __str__(self):
        return self.descripcion 

class Estados(models.Model):
    estado_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=30)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'Estados'
        verbose_name_plural = "Estados"

    def __str__(self):
        return self.descripcion 

class Roles(models.Model):
    rol_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=30)
    tipo_rol = models.CharField(max_length=1, choices = Tipo_rol_choices, default = 'O')
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'Roles'
        verbose_name_plural = "Roles"

class Servicios(models.Model):
    servicio_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50, blank=True, null=True)
    abreviatura = models.CharField(max_length=30, blank=True, null=True)
    fecha_alta = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    usuario_alta = models.CharField(max_length=30, blank=True, null=True)
    fecha_cambio = models.DateTimeField(blank=True, null=True)
    usuario_cambio = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = 'Servicios'
        verbose_name_plural = "Servicios"

    def __str__(self):
        return self.descripcion + ' (id=' + str(self.servicio_id) + ')'

class Usuarios(models.Model):
    usuario_id = models.SmallIntegerField(null=False)
    rol = models.ForeignKey(Roles, models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'Usuarios'
        verbose_name_plural = "Usuarios (Roles)"

class Turnos(models.Model):
    turno_id = models.AutoField(primary_key=True)
    servicio = models.ForeignKey(Servicios, models.CASCADE, blank=True, null=True, verbose_name='Servicio Requerido')
    condicion = models.ForeignKey(Condiciones, models.CASCADE, blank=True, null=True, verbose_name='Condicion Solicitante')
    cliente = models.ForeignKey(Clientes, models.CASCADE, blank=True, null=True, verbose_name='Cliente Solicitante')
    usuario = models.CharField(max_length=25, null=True, default='',verbose_name='Usuario que atendio el turno') 
    estado = models.ForeignKey(Estados, models.CASCADE, blank=True, null=True, verbose_name='Estado')
    nticket = models.CharField(max_length=10, blank=True, null=True, verbose_name='Número del Ticket')
    comentario = models.CharField(max_length=50, blank=True, null=True, verbose_name='Comentario')
    fecha = models.DateField(blank=True, null=True, default=timezone.now, verbose_name='Fecha')
    hora = models.TimeField(blank=True, null=True, default=timezone.now, verbose_name='Hora')
    fecalt = models.DateTimeField(blank=True, null=True, default=timezone.now, verbose_name='Fecha de Alta')
    usralt = models.CharField(max_length=30, blank=True, null=True, verbose_name='Usuario de Alta')

    class Meta:
        db_table = 'Turnos'
        verbose_name_plural = "Turnos"