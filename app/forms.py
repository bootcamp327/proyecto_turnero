from django import forms
from .models import Turnos, Servicios

class TurnoForm(forms.ModelForm):

    class Meta:
        model = Turnos
        fields = ['servicio','condicion','cliente','estado','nticket','comentario','fecha','hora']
