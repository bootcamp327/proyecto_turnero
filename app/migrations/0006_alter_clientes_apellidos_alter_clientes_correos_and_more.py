# Generated by Django 4.1.7 on 2023-02-27 00:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_alter_roles_tipo_rol_alter_servicios_servicio_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientes',
            name='apellidos',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Apellidos'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='correos',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Correo Electronico'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='direccion',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Direccion'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='fecha_alta',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Fecha de Alta'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='fecha_cambio',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Ultima Fecha de Cambio'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='ndocumento',
            field=models.CharField(blank=True, max_length=20, null=True, unique=True, verbose_name='Nº de Documento'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='nombres',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Nombres'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='telefonos',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Telefonos'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='tipo_cliente',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.tipoclientes', verbose_name='Tipo de Cliente'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='tipo_documento',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.tipodocumentos', verbose_name='Tipo de Documento'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='usuario_alta',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Usuario de Alta'),
        ),
        migrations.AlterField(
            model_name='clientes',
            name='usuario_cambio',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Usuario de Ultimo Cambio'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='cliente',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.clientes', verbose_name='Cliente Solicitante'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='comentario',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Comentario del Turno'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='condicion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.condiciones', verbose_name='Condicion Solicitante'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='estado',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.estados', verbose_name='Estado del Turno'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='fecha',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='Fecha del Turno'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='fecha_alta',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Fecha de Alta del Turno'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='hora',
            field=models.TimeField(auto_now_add=True, null=True, verbose_name='Hora del Turno'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='nticket',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Número del Ticket'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='servicio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.servicios', verbose_name='Servicio Requerido'),
        ),
        migrations.AlterField(
            model_name='turnos',
            name='usuario_alta',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Hora de Alta del Turno'),
        ),
    ]
